

/**
 * функция заполнения поля поиска и имитация нажатия на кнопку поиска
 */
function fillForm() {
    const input = document.querySelector('#text');
    const btn = document.querySelector('.mini-suggest__button');
    const commands = [
        new KeyboardEvent('keydown', {bubbles: true}),
        new KeyboardEvent('keypress', {bubbles: true}),
        new KeyboardEvent('keyup', {bubbles: true}),
        new Event('input', {bubbles: true}),
        new Event('change', {bubbles: true})
    ];
    input.value = 'купить лопату';

    //перечисляем команды имитации ввода в поле поиска яндекс
    commands.forEach(elem => {
        input.dispatchEvent(elem);
    });

    btn.click();
}

/**
 * функция на парсинг первых 10-и заголовков и ссылок на них, а также вывода их в консоль
 */
function startParse(){
    const arrLink = document.querySelectorAll('.OrganicTitle-Link');
    for (let i = 0; i < 10; i++){
        console.log(arrLink[i].querySelector('.organic__title').textContent, arrLink[i].href)
    }
}


/**
 * функция отправки скрипта на страницу
 * @param func функция которая должна выполница на странице
 * @param id id страницы на которой нужно выполнить скрипт
 */
function sendScript(func, id){
    chrome.scripting.executeScript({
        target: {tabId: id},
        function: func
    });
}

/**
 *функция на получение активной вкладке в браузере
 * @returns {Promise<*>}
 */
async function getCurrentTab() {
    let queryOptions = { active: true, currentWindow: true };
    let [tab] = await chrome.tabs.query(queryOptions);
    return tab
}


/**
 * создание новой вкладке главной страницы яндекс в браузере
 * @returns {Promise<void>}
 */
async function createTab(){
    const newTab = await chrome.tabs.create({url: "https://yandex.ru/"});
}


//слушатель сообщения от главной кнопки с popup страницы
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.start === "start") {
            createTab();

            //слушатель сообщения со страницы яндекс о полной загрузке
            chrome.runtime.onMessage.addListener(
                function messageTab(request) {
                    if (request.greeting === "loaded") {
                        getCurrentTab().then(currentTab => {
                            if (currentTab.url !== "https://yandex.ru/") {

                                //слушатель перезагрузки страницы
                                chrome.tabs.onUpdated.addListener(function onUpdatedTab(calback) {
                                    sendScript(startParse, currentTab.id);
                                    chrome.runtime.onMessage.removeListener(messageTab);
                                    chrome.tabs.onUpdated.removeListener(onUpdatedTab);
                                });
                            } else {
                                sendScript(fillForm, currentTab.id);
                            }
                        });
                    }
                }
            )
        }
    }
);

