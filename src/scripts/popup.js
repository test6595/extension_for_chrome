
document.addEventListener("DOMContentLoaded", () => {
    const btn = document.getElementById('start');

    //По клику отправляем сообщение в back, о начале поиска
    btn.addEventListener("click", () => {
        chrome.runtime.sendMessage({start: "start"});
    });
});